package com.sheepit.client.datamodel.server;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@NoArgsConstructor @Root(strict = false, name = "request") @ToString public class RequestEndPoint {
	
	@Attribute @Getter private String type;
	
	@Attribute @Getter private String path;
	
	@Attribute(name = "max-period", required = false) @Getter private int maxPeriod;
	
}
