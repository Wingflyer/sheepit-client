package com.sheepit.client.datamodel.server;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@NoArgsConstructor @Root(strict = false, name = "jobvalidate") @ToString public class JobValidation {
	
	@Attribute @Getter private int status;
	
}
