package com.sheepit.client.datamodel.server;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

@NoArgsConstructor @Root(strict = false, name = "renderer") @ToString public class RendererInfos {
	
	@Attribute(name = "md5") @Getter private String md5;
	
	@Attribute(name = "commandline") @Getter private String commandline;
	
	@Attribute(name = "update_method") @Getter private String updateMethod;
	
	@ElementList(name = "chunks") @Getter private List<Chunk> chunks;
	
	@Attribute(name = "size") @Getter private int size;
}
