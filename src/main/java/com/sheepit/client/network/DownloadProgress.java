/*
 * Copyright (C) 2024 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.sheepit.client.network;

import com.sheepit.client.ui.Gui;

public class DownloadProgress {
	// global objects
	private Gui gui;
	
	private String guiPattern = "";
	
	private long total = 0;
	private long partial = 0;
	
	public DownloadProgress(Gui gui) {
		this.gui = gui;
	}
	
	public synchronized void reset(String pattern, long total) {
		this.guiPattern = pattern;
		this.total = total;
		this.partial = 0;
	}
	
	public synchronized void addProgress(long progress) {
		this.partial += progress;
		if (this.total != 0) {
			gui.status(String.format(this.guiPattern + " %.0f %%", 100.0f * this.partial / this.total));
		}
	}
}
