package com.sheepit.client.network;

import com.sheepit.client.datamodel.server.Chunk;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * Download item is a zip file split in partial data (chunk)
 */
@Data @AllArgsConstructor public class DownloadItem {
	private List<Chunk> chunks;
	private long totalSize;
	private String MD5;
	
}
