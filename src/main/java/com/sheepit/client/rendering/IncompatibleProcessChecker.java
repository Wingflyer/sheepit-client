/*
 * Copyright (C) 2024 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.sheepit.client.rendering;

import java.util.TimerTask;

import com.sheepit.client.Client;
import com.sheepit.client.os.OS;
import lombok.Getter;
import oshi.software.os.OSProcess;

public class IncompatibleProcessChecker extends TimerTask {
	
	private final Client client;
	@Getter
	private boolean suspendedDueToOtherProcess;
	
	public IncompatibleProcessChecker(Client client_) {
		this.client = client_;
		this.suspendedDueToOtherProcess = false;
	}
	
	@Override public void run() {
		String search = this.client.getConfiguration().getIncompatibleProcess();
		if (search == null || search.isEmpty()) { // to nothing
			return;
		}
		search = search.toLowerCase();
		
		if (isSearchProcessRunning(search)) {
			if (this.client.getRenderingJob() != null && this.client.getRenderingJob().getRenderProcess().getProcess() != null) {
				this.client.getRenderingJob().incompatibleProcessBlock();
			}
			this.client.suspend();
			this.client.getGui().status("Client paused due to 'incompatible process' feature", true);
			this.suspendedDueToOtherProcess = true;
		}
		else {
			if (this.client.isSuspended() && this.suspendedDueToOtherProcess) {
				// restart the client since the other process has been shutdown
				this.client.resume();
			}
		}
	}
	
	public boolean isRunningCompatibleProcess() {
		String search = this.client.getConfiguration().getIncompatibleProcess();
		if (search == null || search.isEmpty()) { // to nothing
			return false;
		}
		
		return isSearchProcessRunning(search.toLowerCase());
	}
	
	private boolean isSearchProcessRunning(String search) {
		for (OSProcess processInfo : OS.getOS().getProcesses()) {
			String name = processInfo.getName();
			if (name == null || name.isEmpty()) {
				continue;
			}
			
			if (name.toLowerCase().contains(search)) {
				this.client.getLog().debug("IncompatibleProcessChecker(" + search + ") found " + processInfo.getName());
				return true;
			}
		}
		
		return false;
	}
}
