/*
 * Copyright (C) 2010-2014 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.sheepit.client.hardware.cpu;

import com.sheepit.client.logger.Log;
import com.sheepit.client.os.OS;
import com.sheepit.client.os.Windows;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Provides attributes to store various characteristic about the CPU and to detect the CPU architecture and core count
 */
@Getter @Setter @NoArgsConstructor public class CPU {
	public static final int MIN_CORES = Runtime.getRuntime().availableProcessors() > 1 ? 2 : 1;
	private String name;
	private String model;
	private String family;
	/**
	 * The detail is limited to only if it's 32 or 64bit
	 */
	private final String arch = detectArch();
	private static final Log log = Log.getInstance();
	private static Integer logicalCores = -1; // Store core count throughout instances
	
	
	/**
	 * Returns the maximum number of processors available.
	 * On Linux and Mac this functions returns and works exactly like availableProcessors.
	 * @see Runtime#availableProcessors()
	 * On Windows 10 (and earlier) the maximum reported number of processor is never over 64
	 * due to the nature of Processor Groups pre-Windows 11 and that JDK 11 goes not operate NUMA aware by default.
	 * Therefore, on Windows, the system is quieried via powershell about the number logical cores.
	 * in order te reflect what Blender will be able to utilize.
	 * @see <a href="https://learn.microsoft.com/en-us/windows/win32/procthread/processor-groups">Processor Groups</a>
	 * @return the maximum number of processors available; never smaller than one
	 */
	public int cores() {
		// If logicalCores is -1, then haven't run the core-checking script yet
		// Only want to run it once since cores is called multiple times
		if(logicalCores == -1) {
			// Use normal method for systems other than windows, and in case script fails.
			logicalCores = Runtime.getRuntime().availableProcessors();
			
			/* It is worth revisiting if this core detection logic is still needed
			once support for either Windows 10 or OpenJDK 11 has been phased out, because as mentioned in the javadoc
			 JDK12+ may now run NUMA-aware out of the box or Win11+ Proc Groups now properly reflect the entire system*/
			if (OS.getOS() instanceof Windows) {
				ProcessBuilder powershellProcess = new ProcessBuilder("powershell.exe",
					"((Get-CimInstance -ClassName Win32_Processor).NumberOfLogicalProcessors | Measure-Object -Sum ).Sum");
				powershellProcess.redirectErrorStream(true);
				Process process = null;
				
				// Initiate Process and catch any possible IO error
				try {
					process = powershellProcess.start();
				} catch(IOException ex) {
					log.error("CPU::cores Error starting Powershell Process. Stacktrace:" + ex);
				}
				
				if (process != null) {
					
					// Using try with resources. Will automatically close the BufferedReader stream
					try (BufferedReader shellStdOut = new BufferedReader(new InputStreamReader(process.getInputStream()))) {
						String shellLine;
						
						while ((shellLine = shellStdOut.readLine()) != null) {
							// Verify output is a number, and if not print it out since it has errored
							if (shellLine.matches("^[+-]?\\d+$")) {
								logicalCores = Integer.parseInt(shellLine);
							}
							else {
								log.error("CPU::cores Powershell error: " + shellLine);
							}
						}
						
						process.waitFor();
					}
					catch (Exception ex) {
						log.error("CPU::cores Error in Powershell Script. Stacktrace: " + ex);
					}
					finally {
						// Destroy process to prevent memory leaks
						process.destroy();
					}
				}
				else {
					log.error("CPU::cores Error powershell process is NULL! Cannot run script.");
				}
			}
		}
		return logicalCores;
	}
	
	/**
	 * Detects whether the CPU is currently operating in 32 or 64 bit mode
	 */
	private static String detectArch() {
		String arch = System.getProperty("os.arch").toLowerCase();
		switch (arch) {
			case "i386":
			case "i686":
			case "x86":
				return "32bit";
			case "amd64":
			case "x86_64":
			case "x64":
			case "aarch64":
				return "64bit";
			default:
				return null;
		}
	}
	
	/**
	 * Returns if we have any data on the CPU
	 * @return boolean reflecting if all attributes of the CPU are empty
	 */
	public boolean haveData() {
		return this.name != null && this.model != null && this.family != null && this.arch != null;
	}
	
}
