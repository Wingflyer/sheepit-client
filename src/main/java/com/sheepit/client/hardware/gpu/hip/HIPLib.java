package com.sheepit.client.hardware.gpu.hip;

import com.sun.jna.Library;
import com.sun.jna.ptr.IntByReference;

public interface HIPLib extends Library {
	int hipGetDeviceCount(IntByReference numDevices);
}

