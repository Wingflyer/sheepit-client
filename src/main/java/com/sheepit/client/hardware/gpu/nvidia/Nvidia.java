package com.sheepit.client.hardware.gpu.nvidia;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sheepit.client.logger.Log;
import com.sheepit.client.hardware.gpu.GPUDevice;
import com.sheepit.client.hardware.gpu.GPULister;
import com.sheepit.client.os.OS;
import com.sun.jna.Native;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.LongByReference;

public class Nvidia implements GPULister {
	public static String TYPE = "OPTIX";
	
	//https://docs.blender.org/manual/en/3.3/render/cycles/gpu_rendering.html#optix-nvidia
	private static final String MINIMUM_DRIVER_VERSION = "470";

	private String getCudaErrorString(int errornum) {
		Map<Integer, String> cudaerror = new HashMap<>();
		cudaerror.put(0, "cudaSuccess");
		cudaerror.put(1, "cudaErrorInvalidValue");
		cudaerror.put(2, "cudaErrorMemoryAllocation");
		cudaerror.put(3, "cudaErrorInitializationError");
		cudaerror.put(4, "cudaErrorCudartUnloading");
		cudaerror.put(5, "cudaErrorProfilerDisabled");
		cudaerror.put(6, "cudaErrorProfilerNotInitialized");
		cudaerror.put(7, "cudaErrorProfilerAlreadyStarted");
		cudaerror.put(8, "cudaErrorProfilerAlreadyStopped");
		cudaerror.put(9, "cudaErrorInvalidConfiguration");
		cudaerror.put(12, "cudaErrorInvalidPitchValue");
		cudaerror.put(13, "cudaErrorInvalidSymbol");
		cudaerror.put(16, "cudaErrorInvalidHostPointer");
		cudaerror.put(17, "cudaErrorInvalidDevicePointer");
		cudaerror.put(18, "cudaErrorInvalidTexture");
		cudaerror.put(19, "cudaErrorInvalidTextureBinding");
		cudaerror.put(20, "cudaErrorInvalidChannelDescriptor");
		cudaerror.put(21, "cudaErrorInvalidMemcpyDirection");
		cudaerror.put(22, "cudaErrorAddressOfConstant");
		cudaerror.put(23, "cudaErrorTextureFetchFailed");
		cudaerror.put(24, "cudaErrorTextureNotBound");
		cudaerror.put(25, "cudaErrorSynchronizationError");
		cudaerror.put(26, "cudaErrorInvalidFilterSetting");
		cudaerror.put(27, "cudaErrorInvalidNormSetting");
		cudaerror.put(28, "cudaErrorMixedDeviceExecution");
		cudaerror.put(31, "cudaErrorNotYetImplemented");
		cudaerror.put(32, "cudaErrorMemoryValueTooLarge");
		cudaerror.put(34, "cudaErrorStubLibrary");
		cudaerror.put(35, "cudaErrorInsufficientDriver");
		cudaerror.put(36, "cudaErrorCallRequiresNewerDriver");
		cudaerror.put(37, "cudaErrorInvalidSurface");
		cudaerror.put(43, "cudaErrorDuplicateVariableName");
		cudaerror.put(44, "cudaErrorDuplicateTextureName");
		cudaerror.put(45, "cudaErrorDuplicateSurfaceName");
		cudaerror.put(46, "cudaErrorDevicesUnavailable");
		cudaerror.put(49, "cudaErrorIncompatibleDriverContext");
		cudaerror.put(52, "cudaErrorMissingConfiguration");
		cudaerror.put(53, "cudaErrorPriorLaunchFailure");
		cudaerror.put(65, "cudaErrorLaunchMaxDepthExceeded");
		cudaerror.put(66, "cudaErrorLaunchFileScopedTex");
		cudaerror.put(67, "cudaErrorLaunchFileScopedSurf");
		cudaerror.put(68, "cudaErrorSyncDepthExceeded");
		cudaerror.put(69, "cudaErrorLaunchPendingCountExceeded");
		cudaerror.put(98, "cudaErrorInvalidDeviceFunction");
		cudaerror.put(100, "cudaErrorNoDevice");
		cudaerror.put(101, "cudaErrorInvalidDevice");
		cudaerror.put(102, "cudaErrorDeviceNotLicensed");
		cudaerror.put(103, "cudaErrorSoftwareValidityNotEstablished");
		cudaerror.put(127, "cudaErrorStartupFailure");
		cudaerror.put(200, "cudaErrorInvalidKernelImage");
		cudaerror.put(201, "cudaErrorDeviceUninitialized");
		cudaerror.put(205, "cudaErrorMapBufferObjectFailed");
		cudaerror.put(206, "cudaErrorUnmapBufferObjectFailed");
		cudaerror.put(207, "cudaErrorArrayIsMapped");
		cudaerror.put(208, "cudaErrorAlreadyMapped");
		cudaerror.put(209, "cudaErrorNoKernelImageForDevice");
		cudaerror.put(210, "cudaErrorAlreadyAcquired");
		cudaerror.put(211, "cudaErrorNotMapped");
		cudaerror.put(212, "cudaErrorNotMappedAsArray");
		cudaerror.put(213, "cudaErrorNotMappedAsPointer");
		cudaerror.put(214, "cudaErrorECCUncorrectable");
		cudaerror.put(215, "cudaErrorUnsupportedLimit");
		cudaerror.put(216, "cudaErrorDeviceAlreadyInUse");
		cudaerror.put(217, "cudaErrorPeerAccessUnsupported");
		cudaerror.put(218, "cudaErrorInvalidPtx");
		cudaerror.put(219, "cudaErrorInvalidGraphicsContext");
		cudaerror.put(220, "cudaErrorNvlinkUncorrectable");
		cudaerror.put(221, "cudaErrorJitCompilerNotFound");
		cudaerror.put(222, "cudaErrorUnsupportedPtxVersion");
		cudaerror.put(223, "cudaErrorJitCompilationDisabled");
		cudaerror.put(224, "cudaErrorUnsupportedExecAffinity");
		cudaerror.put(225, "cudaErrorUnsupportedDevSideSync");
		cudaerror.put(300, "cudaErrorInvalidSource");
		cudaerror.put(301, "cudaErrorFileNotFound");
		cudaerror.put(302, "cudaErrorSharedObjectSymbolNotFound");
		cudaerror.put(303, "cudaErrorSharedObjectInitFailed");
		cudaerror.put(304, "cudaErrorOperatingSystem");
		cudaerror.put(400, "cudaErrorInvalidResourceHandle");
		cudaerror.put(401, "cudaErrorIllegalState");
		cudaerror.put(402, "cudaErrorLossyQuery");
		cudaerror.put(500, "cudaErrorSymbolNotFound");
		cudaerror.put(600, "cudaErrorNotReady");
		cudaerror.put(700, "cudaErrorIllegalAddress");
		cudaerror.put(701, "cudaErrorLaunchOutOfResources");
		cudaerror.put(702, "cudaErrorLaunchTimeout");
		cudaerror.put(703, "cudaErrorLaunchIncompatibleTexturing");
		cudaerror.put(704, "cudaErrorPeerAccessAlreadyEnabled");
		cudaerror.put(705, "cudaErrorPeerAccessNotEnabled");
		cudaerror.put(708, "cudaErrorSetOnActiveProcess");
		cudaerror.put(709, "cudaErrorContextIsDestroyed");
		cudaerror.put(710, "cudaErrorAssert");
		cudaerror.put(711, "cudaErrorTooManyPeers");
		cudaerror.put(712, "cudaErrorHostMemoryAlreadyRegistered");
		cudaerror.put(713, "cudaErrorHostMemoryNotRegistered");
		cudaerror.put(714, "cudaErrorHardwareStackError");
		cudaerror.put(715, "cudaErrorIllegalInstruction");
		cudaerror.put(716, "cudaErrorMisalignedAddress");
		cudaerror.put(717, "cudaErrorInvalidAddressSpace");
		cudaerror.put(718, "cudaErrorInvalidPc");
		cudaerror.put(719, "cudaErrorLaunchFailure");
		cudaerror.put(720, "cudaErrorCooperativeLaunchTooLarge");
		cudaerror.put(800, "cudaErrorNotPermitted");
		cudaerror.put(801, "cudaErrorNotSupported");
		cudaerror.put(802, "cudaErrorSystemNotReady");
		cudaerror.put(803, "cudaErrorSystemDriverMismatch");
		cudaerror.put(804, "cudaErrorCompatNotSupportedOnDevice");
		cudaerror.put(805, "cudaErrorMpsConnectionFailed");
		cudaerror.put(806, "cudaErrorMpsRpcFailure");
		cudaerror.put(807, "cudaErrorMpsServerNotReady");
		cudaerror.put(808, "cudaErrorMpsMaxClientsReached");
		cudaerror.put(809, "cudaErrorMpsMaxConnectionsReached");
		cudaerror.put(810, "cudaErrorMpsClientTerminated");
		cudaerror.put(811, "cudaErrorCdpNotSupported");
		cudaerror.put(812, "cudaErrorCdpVersionMismatch");
		cudaerror.put(900, "cudaErrorStreamCaptureUnsupported");
		cudaerror.put(901, "cudaErrorStreamCaptureInvalidated");
		cudaerror.put(902, "cudaErrorStreamCaptureMerge");
		cudaerror.put(903, "cudaErrorStreamCaptureUnmatched");
		cudaerror.put(904, "cudaErrorStreamCaptureUnjoined");
		cudaerror.put(905, "cudaErrorStreamCaptureIsolation");
		cudaerror.put(906, "cudaErrorStreamCaptureImplicit");
		cudaerror.put(907, "cudaErrorCapturedEvent");
		cudaerror.put(908, "cudaErrorStreamCaptureWrongThread");
		cudaerror.put(909, "cudaErrorTimeout");
		cudaerror.put(910, "cudaErrorGraphExecUpdateFailure");
		cudaerror.put(911, "cudaErrorExternalDevice");
		cudaerror.put(912, "cudaErrorInvalidClusterSize");
		cudaerror.put(999, "cudaErrorUnknown");
		cudaerror.put(10000, "cudaErrorApiFailureBase");
		
		if (cudaerror.containsKey(errornum)) {
			return cudaerror.get(errornum);
		}
		else {
			return "Unknown(" + errornum + ")";
		}
	}

	@Override public List<GPUDevice> getGpus() {
		OS os = OS.getOS();
		String path = os.getCUDALib();
		if (path == null) {
			return null;
		}
		CUDA cudalib;
		try {
			cudalib = (CUDA) Native.load(path, CUDA.class);
		}
		catch (UnsatisfiedLinkError e) {
			return null;
		}
		catch (ExceptionInInitializerError e) {
			Log.getInstance().error("Nvidia::getGpus ExceptionInInitializerError " + e);
			return null;
		}
		catch (Exception e) {
			Log.getInstance().error("Nvidia::getGpus generic exception " + e);
			return null;
		}
		
		int result = cudalib.cuInit(0);
		if (result != CUresult.CUDA_SUCCESS) {
			Log.getInstance().error("Nvidia::getGpus cuInit failed: " + getCudaErrorString(result) + " (" + result + ")");
			if (result == CUresult.CUDA_ERROR_UNKNOWN) {
				Log.getInstance().error("If you are running Linux, this error is usually due to nvidia kernel module 'nvidia_uvm' not loaded.");
				Log.getInstance().error("Relaunch the application as root or load the module.");
				Log.getInstance().error("Most of time it does fix the issue.");
			}
			return null;
		}
		
		if (result == CUresult.CUDA_ERROR_NO_DEVICE) {
			return null;
		}
		
		IntByReference count = new IntByReference();
		result = cudalib.cuDeviceGetCount(count);
		
		if (result != CUresult.CUDA_SUCCESS) {
			Log.getInstance().error("Nvidia::getGpus cuDeviceGetCount failed (ret: " + CUresult.stringFor(result) + ")");
			return null;
		}
		
		List<GPUDevice> devices = new ArrayList<>(count.getValue());
		
		for (int num = 0; num < count.getValue(); num++) {
			IntByReference aDevice = new IntByReference();
			
			result = cudalib.cuDeviceGet(aDevice, num);
			if (result != CUresult.CUDA_SUCCESS) {
				Log.getInstance().error("Nvidia::getGpus cuDeviceGet failed (ret: " + CUresult.stringFor(result) + ")");
				continue;
			}
			
			IntByReference computeCapabilityMajor = new IntByReference();
			result = cudalib.cuDeviceGetAttribute(computeCapabilityMajor, CUDeviceAttribute.CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MAJOR, aDevice.getValue());
			if (result != CUresult.CUDA_SUCCESS) {
				Log.getInstance().error("Nvidia::getGpus cuDeviceGetAttribute for CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MAJOR failed (ret: " + CUresult.stringFor(result) + ")");
				continue;
			}
			
			if (computeCapabilityMajor.getValue() < 5) {	//Card is Maxwell or older, too old for optix
				continue;
			}
			
			
			IntByReference pciDomainId = new IntByReference();
			IntByReference pciBusId = new IntByReference();
			IntByReference pciDeviceId = new IntByReference();
			result = cudalib.cuDeviceGetAttribute(pciDomainId, CUDeviceAttribute.CU_DEVICE_ATTRIBUTE_PCI_DOMAIN_ID, aDevice.getValue());
			if (result != CUresult.CUDA_SUCCESS) {
				Log.getInstance().error("Nvidia::getGpus cuDeviceGetAttribute for CU_DEVICE_ATTRIBUTE_PCI_DOMAIN_ID failed (ret: " + CUresult.stringFor(result) + ")");
				continue;
			}
			result = cudalib.cuDeviceGetAttribute(pciBusId, CUDeviceAttribute.CU_DEVICE_ATTRIBUTE_PCI_BUS_ID, aDevice.getValue());
			if (result != CUresult.CUDA_SUCCESS) {
				Log.getInstance().error("Nvidia::getGpus cuDeviceGetAttribute for CU_DEVICE_ATTRIBUTE_PCI_BUS_ID failed (ret: " + CUresult.stringFor(result) + ")");
				continue;
			}
			result = cudalib.cuDeviceGetAttribute(pciDeviceId, CUDeviceAttribute.CU_DEVICE_ATTRIBUTE_PCI_DEVICE_ID, aDevice.getValue());
			if (result != CUresult.CUDA_SUCCESS) {
				Log.getInstance().error("Nvidia::getGpus cuDeviceGetAttribute for CU_DEVICE_ATTRIBUTE_PCI_DEVICE_ID failed (ret: " + CUresult.stringFor(result) + ")");
				continue;
			}
			
			byte name[] = new byte[256];
			
			result = cudalib.cuDeviceGetName(name, 256, num);
			if (result != CUresult.CUDA_SUCCESS) {
				Log.getInstance().error("Nvidia::getGpus cuDeviceGetName failed (ret: " + CUresult.stringFor(result) + ")");
				continue;
			}
			
			LongByReference ram = new LongByReference();
			try {
				result = cudalib.cuDeviceTotalMem_v2(ram, num);
			}
			catch (UnsatisfiedLinkError e) {
				// fall back to old function
				result = cudalib.cuDeviceTotalMem(ram, num);
			}
			
			if (result != CUresult.CUDA_SUCCESS) {
				Log.getInstance().error("Nvidia::getGpus cuDeviceTotalMem failed (ret: " + CUresult.stringFor(result) + ")");
				return null;
			}
			
			NVML nvml;
			try {
				nvml = Native.load(os.getNVMLLib(), NVML.class);
			}
			catch (UnsatisfiedLinkError e) {
				Log.getInstance().error("Nvidia::getGpus failed to load NVML library");
				return null;
			}
			
			result = nvml.nvmlInit_v2();
			if (result != NVMLResult.NVML_SUCCESS) {
				Log.getInstance().error("Nvidia::getGpus failed to nvmlInit failed. Returned " + result);
				return null;
			}
			
			short stringLength = 80;
			byte[] driverStringBuffer = new byte[stringLength];
			result = nvml.nvmlSystemGetDriverVersion(driverStringBuffer, stringLength); //The returned char* will never exceed 80 characters according to the docs
			
			if (result != NVMLResult.NVML_SUCCESS) {
				Log.getInstance().error("Nvidia::getGpus failed to retrieve driver version");
				nvml.nvmlShutdown();
				return null;
			}
			
			nvml.nvmlShutdown();
			
			String driverVersion = (new String(driverStringBuffer)).trim();
			boolean driverTooOld = GPUDevice.compareVersions(driverVersion, MINIMUM_DRIVER_VERSION) < 0;
			if (driverTooOld) {
				Log.getInstance().error("Nvidia::getGpus driver version: " + driverVersion + " is too old. Please update to version: " + MINIMUM_DRIVER_VERSION + " or newer." );
				return null;
			}
			
			String blenderId = String.format("CUDA_%s_%04x:%02x:%02x_OptiX", new String(name).trim(), pciDomainId.getValue(), pciBusId.getValue(), pciDeviceId.getValue());
			GPUDevice gpu = new GPUDevice(TYPE, new String(name).trim(), ram.getValue(), blenderId);
			gpu.setDriverVersion(driverVersion);
			// for backward compatibility generate a CUDA_N id
			gpu.setOldId(TYPE + "_" + num);
			devices.add(gpu);
		}
		
		return devices;
	}
}
