/*
 * Copyright (C) 2015 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.sheepit.client.exception;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Server down (server side error) or unreachable (client side error)
 */
public class SheepItServerDown extends SheepItExceptionWithRequiredWait {
	public SheepItServerDown() {
		super();
	}
	
	public SheepItServerDown(String message) {
		super(message);
	}
	
	@Override public String getHumanText() {
		return "Cannot connect to the server. Please check your connectivity. Will try again at %tR";
	}
	
	@Override public int getWaitDuration() {
		return 1000 * 60 * ThreadLocalRandom.current().nextInt(10, 30 + 1);
	}
}
