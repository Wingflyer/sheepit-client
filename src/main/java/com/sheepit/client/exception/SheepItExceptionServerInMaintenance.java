/*
 * Copyright (C) 2016 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.sheepit.client.exception;

import java.util.concurrent.ThreadLocalRandom;

public class SheepItExceptionServerInMaintenance extends SheepItExceptionWithRequiredWait {
	public SheepItExceptionServerInMaintenance() {
		super();
	}
	
	public SheepItExceptionServerInMaintenance(String message) {
		super(message);
	}
	
	@Override public String getHumanText() {
		return "The server is under maintenance and cannot allocate a job. Will try again at %tR";
	}
	
	@Override public int getWaitDuration() {
		return 1000 * 60 * ThreadLocalRandom.current().nextInt(20, 30 + 1);
	}
}
