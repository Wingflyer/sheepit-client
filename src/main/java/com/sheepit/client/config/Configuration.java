/*
 * Copyright (C) 2010-2014 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.sheepit.client.config;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Calendar;
import java.util.List;

import com.sheepit.client.Client;
import com.sheepit.client.hardware.cpu.CPU;
import com.sheepit.client.hardware.gpu.GPUDevice;
import com.sheepit.client.os.OS;
import com.sheepit.client.utils.Pair;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Provides facilities regarding the configuration of the SheepIt client
 */
@AllArgsConstructor
@Data public class Configuration {
	public static final String WORKING_DIRNAME = "sheepit";
	public static final String WOOL_CACHE_DIRNAME = "sheepit_wool_cache";
	
	public static final String jarVersion = getJarVersion();
	
	public enum ComputeType {
		CPU_GPU, CPU, GPU
	} // accept job for ...
	
	private String configFilePath;
	private String logDirectory;
	private File workingDirectory;
	private File sharedDownloadsDirectory;
	private File woolCacheDirectory; // store all the chunks (project and blender)
	private boolean userHasSpecifiedACacheDir;
	private String login;
	private String password;
	private String proxy;
	private int maxUploadingJob;
	private int nbCores;
	private long maxAllowedMemory; // in KiB, max memory allowed for render
	private int maxRenderTime; // max render time per frame allowed
	private int priority;
	private ComputeType computeMethod;
	private GPUDevice GPUDevice;
	private boolean detectGPUs;
	private boolean printLog;
	private boolean debugLevel;
	private List<Pair<Calendar, Calendar>> requestTime;
	private long shutdownTime;
	private String shutdownMode;
	private String extras;
	private boolean autoSignIn;
	private boolean useSysTray;
	private boolean headless;
	private String UIType;
	private String hostname;
	private String theme;
	private boolean disableLargeDownloads;
	private String incompatibleProcess;
	
	public Configuration(File cache_dir_, String login_, String password_) {
		this.configFilePath = null;
		this.logDirectory = null;
		this.login = login_;
		this.password = password_;
		this.proxy = null;
		this.hostname = this.getDefaultHostname();
		this.maxUploadingJob = 1;
		this.nbCores = -1; // ie not set
		this.maxAllowedMemory = -1; // ie not set
		this.maxRenderTime = -1; // ie not set
		this.priority = 19; // default lowest
		this.computeMethod = null;
		this.GPUDevice = null;
		this.userHasSpecifiedACacheDir = false;
		this.detectGPUs = true;
		this.workingDirectory = null;
		this.sharedDownloadsDirectory = null;
		this.woolCacheDirectory = null;
		this.setCacheDir(cache_dir_);
		this.printLog = false;
		this.debugLevel = false;
		this.requestTime = null;
		this.shutdownTime = -1;
		this.shutdownMode = "soft";
		this.extras = "";
		this.autoSignIn = false;
		this.useSysTray = false;
		this.headless = java.awt.GraphicsEnvironment.isHeadless();
		this.UIType = null;
		this.theme = null;
		this.disableLargeDownloads = false;
		this.incompatibleProcess = null;
	}
	
	/**
	 * @return string formatted with SheepIt Admin Log Viewer in mind
	 */
	@Override public String toString() {
		String c = "    CFG: ";
		String n ="\n";
		return
				c + "version:                   " + getJarVersion() + n +
				c + "configFilePath:            " + configFilePath + n +
				c + "logDir:                    " + (logDirectory == null ? "NULL" : logDirectory) + n +
				c + "workingDirectory:          " + workingDirectory + n +
				c + "sharedDownloadsDirectory:  " + sharedDownloadsDirectory + n +
				c + "woolCacheDirectory:        " + woolCacheDirectory + n +
				c + "userHasSpecifiedACacheDir: " + userHasSpecifiedACacheDir + n +
				c + "login:                     " + login + n +
				c + "proxy:                     " + proxy + n +
				c + "maxUploadingJob:           " + maxUploadingJob + n +
				c + "nbCores:                   " + nbCores + n +
				c + "maxAllowedMemory:          " + maxAllowedMemory + n +
				c + "maxRenderTime:             " + maxRenderTime + n +
				c + "priority:                  " + priority + n +
				c + "computeMethod:             " + computeMethod + n +
				c + "GPUDevice:                 " + GPUDevice + n +
				c + "detectGPUs:                " + detectGPUs + n +
				c + "printLog:                  " + printLog + n +
				c + "debugLog:                  " + debugLevel + n +
				c + "requestTime:               " + requestTime + n +
				c + "shutdownTime:              " + shutdownTime + n +
				c + "shutdownMode:              " + shutdownMode + n +
				c + "extras:                    " + extras + n +
				c + "autoSignIn:                " + autoSignIn + n +
				c + "useSysTray:                " + useSysTray + n +
				c + "headless:                  " + headless + n +
				c + "UIType:                    " + UIType + n +
				c + "hostname:                  " + hostname + n +
				c + "theme:                     " + theme + n +
				c + "incompatibleProcess:       " + incompatibleProcess + n +
				c + "disableLargeDownloads:      " + disableLargeDownloads;
	}
	
	/**
	 * Sets the priority that Blender will be started with
	 * @param priority integer that will be clamped between 19 and -19,
	 *                 lowest to highest in terms of priority
	 */
	public void setPriority(int priority) {
		if (priority > 19)
			priority = 19;
		else if (priority < -19)
			priority = -19;
		
		this.priority = priority;
	}
	
	/**
	 * Returns ordinal integer of the computeMethod enum
	 * @return ordinal integer of the computeMethod enum
	 */
	public int computeMethodToInt() {
		return this.computeMethod.ordinal();
	}
	
	/**
	 * Sets and optionally creates cache directory
	 * @param cache_dir_ File representing cache directory
	 */
	public void setCacheDir(File cache_dir_) {
		if (cache_dir_ == null) {
			this.userHasSpecifiedACacheDir = false;
			try {
				this.workingDirectory = File.createTempFile("farm_", "");
				this.workingDirectory.createNewFile(); // hoho...
				this.workingDirectory.delete(); // hoho
			}
			catch (IOException e) {
				e.printStackTrace();
			}

			// since there is no working directory and the client will be working in the system temp directory,
			// we can also set up a 'permanent' directory for immutable files (like renderer binary)
			this.woolCacheDirectory = new File(this.workingDirectory.getParent() + File.separator + WOOL_CACHE_DIRNAME);
		
		}
		else {
			this.userHasSpecifiedACacheDir = true;
			this.workingDirectory = new File(cache_dir_.getAbsolutePath() + File.separator + WORKING_DIRNAME);
			this.woolCacheDirectory = new File(cache_dir_.getAbsolutePath() + File.separator + WOOL_CACHE_DIRNAME);
		}
	}
	
	/**
	 * @return the user specified cache directory or null if it hasn't been specified
	 */
	public File getCacheDirForSettings() {
		// when the user has a cache directory specified,
		// a "sheepit" and "wool_cache" directory is to be automatically created
		return this.userHasSpecifiedACacheDir == false ? null : this.workingDirectory.getParentFile();
	}
	
	/**
	 * @return string-representation of the hostname or an empty string if an error was encountered
	 */
	public String getDefaultHostname() {
		try {
			return InetAddress.getLocalHost().getHostName();
		}
		catch (UnknownHostException e) {
			return "";
		}
	}
	
	/**
	 * @return a string representing the version of the jar or
	 *         {@code 6.0.0} if on error is encountered or the VERSION file doesn't exist
	 */
	private static String getJarVersion() {
		String versionPath = "/VERSION";
		String version = "6.0.0";
		
		InputStream versionStream = Client.class.getResourceAsStream(versionPath);
		if (versionStream != null) {
			try {
				BufferedReader in = new BufferedReader(new InputStreamReader(versionStream));
				version = in.readLine();
			}
			catch (IOException ex) {
				System.err.println("Configuration::getJarVersion error while reading manifest file (" + versionPath + "): " + ex.getMessage());
			}
		}
		else {
			System.err.println("Configuration::getJarVersion Failed to get version file");
		}
		return version;
	}
	
	/**
	 * @return true if operating system is in among the supported platforms
	 *         and is running in a compatible configuration
	 *         false otherwise
	 * @see OS#isSupported()
	 */
	public boolean checkOSisSupported() {
		return OS.getOS() != null && OS.getOS().isSupported();
	}
	
	/**
	 * @return true if operating system is in among the supported platforms
	 *         and if we have data on the cpu
	 *         false otherwise
	 * @see CPU#haveData()
	 */
	public boolean checkCPUisSupported() {
		OS os = OS.getOS();
		if (os != null) {
			CPU cpu = os.getCPU();
			return cpu != null && cpu.haveData();
		}
		return false;
	}
}
